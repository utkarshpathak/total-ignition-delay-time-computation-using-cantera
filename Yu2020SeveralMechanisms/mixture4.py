from __future__ import division
from __future__ import print_function
import numpy as np
import time
import cantera as ct
print('Running Cantera version: ' + ct.__version__)

#----------------------------------------------------------
#Case data
numberofreadingsformixture = 11; #change
MixtureTemperature = np.zeros(numberofreadingsformixture, 'd')
MixtureTemperature = [638, 659.6, 685.7, 715.2, 743, 770.9, 802.9, 833.5, 863.2, 890.1, 919.8] #change
idt_exp = [147, 59.4, 31.1, 17.2, 17, 20.7, 29.3, 34.8, 37.9, 32.3, 21.8] #change

#----------------------------------------------------------
#Plotting
import matplotlib.pyplot as plt
plt.rcParams["figure.figsize"] = (10,6)
plt.yscale("log")
temp = np.zeros(numberofreadingsformixture,'d')
for i in range(numberofreadingsformixture):
	temp[i] = 1000/MixtureTemperature[i]
plt.plot(temp,idt_exp, 'o', color = 'black', label = 'Experiment')

plotshape = ['X', 'v', 's', '*', '^', 'o']
plotcolour = ['blue', 'red', 'green', 'grey', 'orange', 'cyan']
#----------------------------------------------------------

#Mechanism used for the process
mechanism = ['cs1.cti', 'cs2.cti', 'csh1.cti', 'ps1.cti', 'psh1.cti']
mechname = ['Chalmers-Stagni-1', 'Chalmers-Stagni-2', 'Chalmers-Shreshtha-1', 'Pichler-Stagni-1', 'Pichler-Shreshtha-1']

mechcount = 0
for mech in mechanism:
    gas = ct.Solution(mech)

    Autoignitionvalues = np.zeros(numberofreadingsformixture,'d')
    reading = 0
    print('\n\nSimulating for mechanism: ' + mech + '\n\n')

    for gastemp in MixtureTemperature:

        #Initial temperature, Pressure and stoichiometry
        gas.TPX = gastemp, 15.0*100000, 'NH3:0.098,NC7H16:0.0103,O2:0.0937,AR:0.798324' #change

        #Specify the number of time steps and the time step size
        nt = 1200 * 1000
        dt = 1.e-7 #s

        #Storage space
        mfrac = np.zeros(gas.n_species,'d')
        tim = np.zeros(nt,'d')
        pres = np.zeros(nt,'d')
        dpres = np.zeros(nt-1,'d')

        #Create the batch reactor and fill it with the gas
        r = ct.IdealGasReactor(contents=gas, name='Batch Reactor')

        #Now create a reactor network consisting of the single batch reactor
        sim = ct.ReactorNet([r])

        #Run the simulation

        # Initial simulation time
        time = 0.0

        #Loop for nt time steps of dt seconds.
        for n in range(nt):
            time += dt
            sim.advance(time)

            #save the time and pressure of each time step
            tim[n] = time
            pres[n] = r.thermo.P
            #print('Time step = ', n)

        #and save the final mass fraction of species
        mfrac[:] = r.thermo.Y

        # Catch the autoignition timing

        Dpmax = [0,0.0]
        for n in range(nt-1):
            dpres[n] = (pres[n+1]-pres[n])/dt
            if (dpres[n]>Dpmax[1]):
                Dpmax[0] = n
                Dpmax[1] = dpres[n]

        # Local print
        Autoignition = 1000.0 * (tim[Dpmax[0]]+tim[Dpmax[0] + 1])/2.
        #print ('Autoignition time (s) = ' + str(Autoignition) + ' for Gas temp. = ' + str(gastemp))
        Autoignitionvalues[reading] = Autoignition
        reading += 1
    print(Autoignitionvalues)
    print('\n\n\n\n\n')

    #plot the values
    plt.plot(temp, Autoignitionvalues, plotshape[mechcount], color=plotcolour[mechcount], label=mechname[mechcount])
    mechcount = mechcount + 1

#----------------------------------------------------------
#Rest of the Plotting
plt.xlabel(r'1000/T(K)', fontsize=16)
plt.ylabel("IDT (ms)", fontsize=16)
plt.title(r'IDT vs. Temp. (Mixture 4: P = 15 bar, $\phi$ = 2, $\mathregular{X_{NH3}}$ = 9.8%, $\mathregular{X_{C7H16}}$ = 1.03%, $\mathregular{X_{O2}}$ = 9.37%)',fontsize=12, horizontalalignment='center') #change
plt.axis([1.0,1.6,1.0,10000.0]) #change
plt.grid()
plt.legend()

#show()
plt.savefig('plot.png', bbox_inches='tight')

print('EOF')
