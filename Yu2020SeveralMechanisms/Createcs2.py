from __future__ import division
from __future__ import print_function
import numpy as np
import time
import cantera as ct
print('Running Cantera version: ' + ct.__version__)

#----------------------------------------------------------
#Case data
numberofreadingsformixture = 12;
MixtureTemperature = np.zeros(numberofreadingsformixture, 'd')
MixtureTemperature = [635.5, 656.8, 677.8, 700.1, 721.5, 746.9, 771.5, 801.1, 832.2, 857.2, 888.2, 915.5]
idt_exp = [117.4, 47.3, 24.3, 13.4, 10.7, 11.5, 16.1, 28.4, 34.5, 31.2, 30.8, 22.7]

#----------------------------------------------------------
#Plotting
import matplotlib.pyplot as plt
plt.rcParams["figure.figsize"] = (10,6)
plt.yscale("log")
temp = np.zeros(numberofreadingsformixture,'d')
for i in range(numberofreadingsformixture):
	temp[i] = 1000/MixtureTemperature[i]
plt.plot(temp,idt_exp, 'o', color = 'black', label = 'Experiment')

plotshape = ['X', 'v', 's', '*', '^']
plotcolour = ['blue', 'red', 'green', 'grey', 'orange']
#----------------------------------------------------------

#Mechanism used for the process
mechanism = ['csh1.cti']
#mechanism = ['cs1.cti', 'cs2.cti']
#mechname = ['Chalmers-Stagni-1', 'Chalmers-Stagni-2']

mechcount = 0
for mech in mechanism:
    gas = ct.Solution(mech)

    Autoignitionvalues = np.zeros(numberofreadingsformixture,'d')
    reading = 0
    print('\n\nSimulating for mechanism: ' + mech + '\n\n')

    for gastemp in MixtureTemperature:

        #Initial temperature, Pressure and stoichiometry
        gas.TPX = gastemp, 15.0*100000, 'NH3:0.0264,NC7H16:0.0074,O2:0.1015,AR:0.86478'

        #Specify the number of time steps and the time step size
        nt = 1200 * 1000
        dt = 1.e-6 #s

        #Storage space
        mfrac = np.zeros(gas.n_species,'d')
        tim = np.zeros(nt,'d')
        pres = np.zeros(nt,'d')
        dpres = np.zeros(nt-1,'d')

        #Create the batch reactor and fill it with the gas
        r = ct.IdealGasReactor(contents=gas, name='Batch Reactor')

        #Now create a reactor network consisting of the single batch reactor
        sim = ct.ReactorNet([r])

        #Run the simulation

        # Initial simulation time
        time = 0.0

        #Loop for nt time steps of dt seconds.
        for n in range(nt):
            time += dt
            sim.advance(time)

            #save the time and pressure of each time step
            tim[n] = time
            pres[n] = r.thermo.P
            #print('Time step = ', n)

        #and save the final mass fraction of species
        mfrac[:] = r.thermo.Y

        # Catch the autoignition timing

        Dpmax = [0,0.0]
        for n in range(nt-1):
            dpres[n] = (pres[n+1]-pres[n])/dt
            if (dpres[n]>Dpmax[1]):
                Dpmax[0] = n
                Dpmax[1] = dpres[n]

        # Local print
        Autoignition = 1000.0 * (tim[Dpmax[0]]+tim[Dpmax[0] + 1])/2.
        #print ('Autoignition time (s) = ' + str(Autoignition) + ' for Gas temp. = ' + str(gastemp))
        Autoignitionvalues[reading] = Autoignition
        reading += 1
    print(Autoignitionvalues)
    print('\n\n\n\n\n')

    #plot the values
    plt.plot(temp, Autoignitionvalues, plotshape[mechcount], color=plotcolour[mechcount], label=mechname[mechcount])
    mechcount = mechcount + 1

#----------------------------------------------------------
#Rest of the Plotting
plt.xlabel(r'1000/T(K)', fontsize=16)
plt.ylabel("IDT (ms)", fontsize=16)
plt.title(r'IDT vs. Temp. (Mixture 2: P = 15 bar, $\phi$ = 1, $\mathregular{X_{NH3}}$ = 2.64%, $\mathregular{X_{C7H16}}$ = 0.74%, $\mathregular{X_{O2}}$ = 10.15%)',fontsize=12, horizontalalignment='center')
plt.axis([1.05,1.6,1.0,10000.0])
plt.grid()
plt.legend()

#show()
plt.savefig('plot.png', bbox_inches='tight')

print('EOF')
