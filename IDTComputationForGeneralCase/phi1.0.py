from __future__ import division
from __future__ import print_function
import numpy as np
import time
import cantera as ct
print('Running Cantera version: ' + ct.__version__)

#----------------------------------------------------------
#Case data
numberofreadingsformixture = 15; #change
#MixtureTemperature = np.linspace(650.0, 950.0, num = numberofreadingsformixture)
MixtureTemperature = np.zeros(numberofreadingsformixture, 'd')
MixtureTemperature = [675.0, 700.0, 750.0, 775.0, 800.0, 815.0, 830.0, 845.0, 860.0, 875.0, 890.0, 905.0, 920.0, 935.0, 950.0] #change
print(MixtureTemperature)
timestep1 = [1.e-6, 1.e-6, 1.e-6, 1.e-6, 1.e-6, 1.e-6, 1.e-6, 1.e-6, 1.e-6, 1.e-6, 1.e-6, 1.e-6, 1.e-6, 1.e-6, 1.e-6]
timestep2 = [1.e-6, 1.e-6, 1.e-6, 1.e-6, 1.e-6, 1.e-6, 1.e-6, 1.e-6, 1.e-6, 1.e-6, 1.e-6, 1.e-6, 1.e-6, 1.e-6, 1.e-6]
gaspres = 75.0
phi = 1.0

#----------------------------------------------------------
#Plotting
import matplotlib.pyplot as plt
plt.rcParams["figure.figsize"] = (6,5)
plt.yscale("log")
temp = np.zeros(numberofreadingsformixture,'d')
for i in range(numberofreadingsformixture):
	temp[i] = 1000/MixtureTemperature[i]
#plt.plot(temp,idt_exp, 'o', color = 'black', label = 'Experiment')

#Mechanism used for the process
mechanism = ['fixedmech.cti', 'liu.cti']
mechname = ['Chalmers-Stagni (AmmoniaMot)', 'Liu 2021']

#mechanism = ['c53s1.cti']
#mechname = ['Reduced Chalmers-Stagni']

plotname = ['$\mathregular{E_{NH3}}$ = 0%', '$\mathregular{E_{NH3}}$ = 10%', '$\mathregular{E_{NH3}}$ = 20%']
plotcolour = ['blue', 'red', 'black']

# E0
#----------------------------------------------------------


plotshape = ['-', '-s']


mechcount = 0
for mech in mechanism:
    gas = ct.Solution(mech)

    Autoignitionvalues = np.zeros(numberofreadingsformixture,'d')
    reading = 0
    print('\n\nSimulating for mechanism: ' + mech + '\n\n')
    
    tempcount = 0

    for gastemp in MixtureTemperature:

        fuel = "NH3:0.0, NC7H16:1.0"
        oxidizer = "O2:1.0, N2:3.76"

		# Gas conditions
        gas.TP = gastemp, gaspres*100000
        gas.set_equivalence_ratio(phi, fuel, oxidizer)

        print('\n\n\n\n\n')
        equivalenceratio = gas.get_equivalence_ratio()
        print('The equivalence ratio is: ' + str(equivalenceratio))
        print('\n\n\n\n\n')

        #Specify the number of time steps and the time step size
        nt = 1200 * 1000
        dt = 1.e-6
        if(mechcount == 0):
            dt = timestep1[tempcount] #s
        else:
            dt = timestep2[tempcount]
        tempcount = tempcount + 1
        
        #Storage space
        mfrac = np.zeros(gas.n_species,'d')
        tim = np.zeros(nt,'d')
        pres = np.zeros(nt,'d')
        dpres = np.zeros(nt-1,'d')

        #Create the batch reactor and fill it with the gas
        r = ct.IdealGasReactor(contents=gas, name='Batch Reactor')

        #Now create a reactor network consisting of the single batch reactor
        sim = ct.ReactorNet([r])

        #Run the simulation

        # Initial simulation time
        time = 0.0

        #Loop for nt time steps of dt seconds.
        for n in range(nt):
            time += dt
            sim.advance(time)

            #save the time and pressure of each time step
            tim[n] = time
            pres[n] = r.thermo.P
            #print('Time step = ', n)

        #and save the final mass fraction of species
        mfrac[:] = r.thermo.Y

        # Catch the autoignition timing

        Dpmax = [0,0.0]
        for n in range(nt-1):
            dpres[n] = (pres[n+1]-pres[n])/dt
            if (dpres[n]>Dpmax[1]):
                Dpmax[0] = n
                Dpmax[1] = dpres[n]

        # Local print
        Autoignition = 1000.0 * (tim[Dpmax[0]]+tim[Dpmax[0] + 1])/2.
        #print ('Autoignition time (s) = ' + str(Autoignition) + ' for Gas temp. = ' + str(gastemp))
        Autoignitionvalues[reading] = Autoignition
        reading += 1
    print(Autoignitionvalues)
    print('\n\n\n\n\n')

    #plot the values
    plt.plot(temp, Autoignitionvalues, plotshape[mechcount], color=plotcolour[0], label=mechname[mechcount] + ', ' + plotname[0])
    mechcount = mechcount + 1

#----------------------------------------------------------

# E10
#----------------------------------------------------------


plotshape = ['-', '-s']


mechcount = 0
for mech in mechanism:
    gas = ct.Solution(mech)

    Autoignitionvalues = np.zeros(numberofreadingsformixture,'d')
    reading = 0
    print('\n\nSimulating for mechanism: ' + mech + '\n\n')
    
    tempcount = 0

    for gastemp in MixtureTemperature:

        fuel = "NH3:1.0, NC7H16:0.704089"
        oxidizer = "O2:1.0, N2:3.76"

		# Gas conditions
        gas.TP = gastemp, gaspres*100000
        gas.set_equivalence_ratio(phi, fuel, oxidizer)

        print('\n\n\n\n\n')
        equivalenceratio = gas.get_equivalence_ratio()
        print('The equivalence ratio is: ' + str(equivalenceratio))
        print('\n\n\n\n\n')

        #Specify the number of time steps and the time step size
        nt = 1200 * 1000
        dt = 1.e-6
        if(mechcount == 0):
            dt = timestep1[tempcount] #s
        else:
            dt = timestep2[tempcount]
        tempcount = tempcount + 1

        #Storage space
        mfrac = np.zeros(gas.n_species,'d')
        tim = np.zeros(nt,'d')
        pres = np.zeros(nt,'d')
        dpres = np.zeros(nt-1,'d')

        #Create the batch reactor and fill it with the gas
        r = ct.IdealGasReactor(contents=gas, name='Batch Reactor')

        #Now create a reactor network consisting of the single batch reactor
        sim = ct.ReactorNet([r])

        #Run the simulation

        # Initial simulation time
        time = 0.0

        #Loop for nt time steps of dt seconds.
        for n in range(nt):
            time += dt
            sim.advance(time)

            #save the time and pressure of each time step
            tim[n] = time
            pres[n] = r.thermo.P
            #print('Time step = ', n)

        #and save the final mass fraction of species
        mfrac[:] = r.thermo.Y

        # Catch the autoignition timing

        Dpmax = [0,0.0]
        for n in range(nt-1):
            dpres[n] = (pres[n+1]-pres[n])/dt
            if (dpres[n]>Dpmax[1]):
                Dpmax[0] = n
                Dpmax[1] = dpres[n]

        # Local print
        Autoignition = 1000.0 * (tim[Dpmax[0]]+tim[Dpmax[0] + 1])/2.
        #print ('Autoignition time (s) = ' + str(Autoignition) + ' for Gas temp. = ' + str(gastemp))
        Autoignitionvalues[reading] = Autoignition
        reading += 1
    print(Autoignitionvalues)
    print('\n\n\n\n\n')

    #plot the values
    plt.plot(temp, Autoignitionvalues, plotshape[mechcount], color=plotcolour[1], label=mechname[mechcount] + ', ' + plotname[1])
    mechcount = mechcount + 1

#----------------------------------------------------------

# E20
#----------------------------------------------------------


plotshape = ['-', '-s']


mechcount = 0
for mech in mechanism:
    gas = ct.Solution(mech)

    Autoignitionvalues = np.zeros(numberofreadingsformixture,'d')
    reading = 0
    print('\n\nSimulating for mechanism: ' + mech + '\n\n')
    
    tempcount = 0

    for gastemp in MixtureTemperature:

        fuel = "NH3:1.0, NC7H16:0.3520445"
        oxidizer = "O2:1.0, N2:3.76"

		# Gas conditions
        gas.TP = gastemp, gaspres*100000
        gas.set_equivalence_ratio(phi, fuel, oxidizer)

        print('\n\n\n\n\n')
        equivalenceratio = gas.get_equivalence_ratio()
        print('The equivalence ratio is: ' + str(equivalenceratio))
        print('\n\n\n\n\n')

        #Specify the number of time steps and the time step size
        nt = 1200 * 1000
        dt = 1.e-6
        if(mechcount == 0):
            dt = timestep1[tempcount] #s
        else:
            dt = timestep2[tempcount]
        tempcount = tempcount + 1

        #Storage space
        mfrac = np.zeros(gas.n_species,'d')
        tim = np.zeros(nt,'d')
        pres = np.zeros(nt,'d')
        dpres = np.zeros(nt-1,'d')

        #Create the batch reactor and fill it with the gas
        r = ct.IdealGasReactor(contents=gas, name='Batch Reactor')

        #Now create a reactor network consisting of the single batch reactor
        sim = ct.ReactorNet([r])

        #Run the simulation

        # Initial simulation time
        time = 0.0

        #Loop for nt time steps of dt seconds.
        for n in range(nt):
            time += dt
            sim.advance(time)

            #save the time and pressure of each time step
            tim[n] = time
            pres[n] = r.thermo.P
            #print('Time step = ', n)

        #and save the final mass fraction of species
        mfrac[:] = r.thermo.Y

        # Catch the autoignition timing

        Dpmax = [0,0.0]
        for n in range(nt-1):
            dpres[n] = (pres[n+1]-pres[n])/dt
            if (dpres[n]>Dpmax[1]):
                Dpmax[0] = n
                Dpmax[1] = dpres[n]

        # Local print
        Autoignition = 1000.0 * (tim[Dpmax[0]]+tim[Dpmax[0] + 1])/2.
        #print ('Autoignition time (s) = ' + str(Autoignition) + ' for Gas temp. = ' + str(gastemp))
        Autoignitionvalues[reading] = Autoignition
        reading += 1
    print(Autoignitionvalues)
    print('\n\n\n\n\n')

    #plot the values
    plt.plot(temp, Autoignitionvalues, plotshape[mechcount], color=plotcolour[2], label=mechname[mechcount] + ', ' + plotname[2])
    mechcount = mechcount + 1

#----------------------------------------------------------



#Rest of the Plotting
plt.xlabel(r'1000/T(K)', fontsize=16)
plt.ylabel("Total Ignition Delay Time (ms)", fontsize=16)
plt.title(r'Total IDT vs. Temp. (' + str(gaspres) +' bar, $\phi$ = '+str(phi)+')',fontsize=12,horizontalalignment='center') #change
plt.axis([1.0,1.5,0.1,100.0]) #change
plt.grid()
plt.legend(loc="upper left", prop={'size': 9})

#show()
plt.savefig('Phi_'+str(phi)+'_P_'+str(gaspres)+'_IDT_'+mechname[1]+'.png', bbox_inches='tight')

print('EOF')
