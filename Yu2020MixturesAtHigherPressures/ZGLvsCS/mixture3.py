from __future__ import division
from __future__ import print_function
import numpy as np
import time
import cantera as ct
print('Running Cantera version: ' + ct.__version__)

#----------------------------------------------------------
#Case data
numberofreadingsformixture = 15; #change
#MixtureTemperature = np.linspace(650.0, 950.0, num = numberofreadingsformixture)
MixtureTemperature = np.zeros(numberofreadingsformixture, 'd')
MixtureTemperature = [650.0, 700.0, 750.0, 775.0, 800.0, 815.0, 830.0, 845.0, 860.0, 875.0, 890.0, 905.0, 920.0, 935.0, 950.0] #change
print(MixtureTemperature)
timestep = [1.e-6, 1.e-6, 1.e-6, 1.e-6, 1.e-6] #all -6 except cn1 which is -9 for the mix. temp. shown above.

#----------------------------------------------------------
#Plotting
import matplotlib.pyplot as plt
plt.rcParams["figure.figsize"] = (10,6)
plt.yscale("log")
temp = np.zeros(numberofreadingsformixture,'d')
for i in range(numberofreadingsformixture):
	temp[i] = 1000/MixtureTemperature[i]
#plt.plot(temp,idt_exp, 'o', color = 'black', label = 'Experiment')

plotshape = ['-', '-', '-', '-', '-', '-P', '-X']
plotcolour = ['blue', 'red', 'green', 'grey', 'orange', 'brown', 'red']
#----------------------------------------------------------

#Mechanism used for the process
mechanism = ['cs1.cti', 'zgl.cti']
mechname = ['Chalmers-Stagni', 'Zhang-Glarborg']


mechcount = 0
for mech in mechanism:
    gas = ct.Solution(mech)

    Autoignitionvalues = np.zeros(numberofreadingsformixture,'d')
    reading = 0
    print('\n\nSimulating for mechanism: ' + mech + '\n\n')

    for gastemp in MixtureTemperature:

        #Initial temperature, Pressure and stoichiometry
        gas.TPX = gastemp, 125.0*100000, 'NH3:0.0518,NC7H16:0.0055,O2:0.099,AR:0.84348' #change

        #Specify the number of time steps and the time step size
        nt = 3000 * 1000
        dt = timestep[mechcount] #s

        #Storage space
        mfrac = np.zeros(gas.n_species,'d')
        tim = np.zeros(nt,'d')
        pres = np.zeros(nt,'d')
        dpres = np.zeros(nt-1,'d')

        #Create the batch reactor and fill it with the gas
        r = ct.IdealGasReactor(contents=gas, name='Batch Reactor')

        #Now create a reactor network consisting of the single batch reactor
        sim = ct.ReactorNet([r])

        #Run the simulation

        # Initial simulation time
        time = 0.0

        #Loop for nt time steps of dt seconds.
        for n in range(nt):
            time += dt
            sim.advance(time)

            #save the time and pressure of each time step
            tim[n] = time
            pres[n] = r.thermo.P
            #print('Time step = ', n)

        #and save the final mass fraction of species
        mfrac[:] = r.thermo.Y

        # Catch the autoignition timing

        Dpmax = [0,0.0]
        for n in range(nt-1):
            dpres[n] = (pres[n+1]-pres[n])/dt
            if (dpres[n]>Dpmax[1]):
                Dpmax[0] = n
                Dpmax[1] = dpres[n]

        # Local print
        Autoignition = 1000.0 * (tim[Dpmax[0]]+tim[Dpmax[0] + 1])/2.
        #print ('Autoignition time (s) = ' + str(Autoignition) + ' for Gas temp. = ' + str(gastemp))
        Autoignitionvalues[reading] = Autoignition
        reading += 1
    print(Autoignitionvalues)
    print('\n\n\n\n\n')

    #plot the values
    plt.plot(temp, Autoignitionvalues, plotshape[mechcount], color=plotcolour[mechcount], label=mechname[mechcount])
    mechcount = mechcount + 1

#----------------------------------------------------------
#Rest of the Plotting
plt.xlabel(r'1000/T(K)', fontsize=16)
plt.ylabel("IDT (ms)", fontsize=16)
plt.title(r'IDT vs. Temp. (Mixture 3: P = 125 bar, $\phi$ = 1, $\mathregular{X_{NH3}}$ = 5.18%, $\mathregular{X_{C7H16}}$ = 0.55%, $\mathregular{X_{O2}}$ = 9.90%)',fontsize=12, horizontalalignment='center') #change
plt.axis([1.0,1.6,0.1,10000.0]) #change
plt.grid()
plt.legend()

#show()
plt.savefig('3.png', bbox_inches='tight')

print('EOF')
